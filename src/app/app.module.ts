import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WEEK_ICONS_PATH } from '@weekkit/web-components';

import { AppComponent } from './app.component';
import { ScreensModule } from './screens/screens.module';

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, ScreensModule],
    providers: [{ provide: WEEK_ICONS_PATH, useValue: './assets/icons/' }],
    bootstrap: [AppComponent],
})
export class AppModule {}
