import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { WeekButtonCommand, WeekCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-create-chat',
    templateUrl: './create-chat.component.html',
    styleUrls: ['./create-chat.component.scss'],
})
export class CreateChatComponent implements OnInit {
    searchControl!: FormControl;
    searchClearCommand?: WeekButtonCommand;
    isSearchClearVisible!: boolean;

    backCommand!: WeekButtonCommand;
    clickCommand!: WeekButtonCommand;

    get contacts(): Contact[] {
        const contacts: Contact[] = [];
        for (let i: number = 0; i < 10; i++) {
            contacts.push({
                user: {
                    id: i,
                    name: 'Кашуба Дмитро Анатолійович',
                    isOnline: false,
                    position: 'програміт прикладний провідний',
                },
                command: {
                    key: `select-${i}`,
                    handler: (resolve: WeekCommandCallback) => {
                        resolve();
                    },
                },
            });
        }
        return contacts;
    }

    constructor() {}

    ngOnInit(): void {
        this._initSearch();
        this._initBackCommand();
    }

    private _initSearch(): void {
        this.searchControl = new FormControl();
        this.searchControl.valueChanges
            .pipe(debounceTime(1000))
            .subscribe((value: string) => (this.isSearchClearVisible = !!value));
        this.searchClearCommand = {
            key: 'clear',
            icon: 'clear',
            tooltip: 'Clear',
            isDisabled: false,
            handler: (resolve: WeekCommandCallback) => {
                resolve(true);
                console.log('Search value:', this.searchControl.value);
                this.searchControl.setValue(null);
            },
        };
    }

    private _initBackCommand(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }
}

export interface User {
    id: number;
    name: string;
    isOnline?: boolean;
    position?: string;
}

export interface Contact {
    user: User;
    command: WeekCommand;
}
