import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-support',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.scss'],
})
export class SupportComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    chatCommand!: WeekButtonCommand;
    copyCommand!: WeekButtonCommand;

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.chatCommand = {
            key: 'update',
            text: 'НАПИСАТИ ПОВІДОМЛЕННЯ',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.copyCommand = {
            key: 'copy',
            icon: 'file_copy',
            tooltip: 'Копіювати',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }
}
