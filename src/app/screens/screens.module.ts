import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { ChatsComponent } from './chats/chats.component';
import { CreateChatComponent } from './create-chat/create-chat.component';
import { UserComponent } from './user/user.component';
import { UserChatComponent } from './user-chat/user-chat.component';
import { CreateGroupContactsComponent } from './create-group-contacts/create-group-contacts.component';
import { CreateGroupDescriptionComponent } from './create-group-description/create-group-description.component';
import { GroupComponent } from './group/group.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { AddToGroupComponent } from './add-to-group/add-to-group.component';
import { MenuComponent } from './menu/menu.component';
import { LanguageComponent } from './language/language.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { VersionComponent } from './version/version.component';
import { SupportComponent } from './support/support.component';
import { UpdateComponent } from './update/update.component';
import { NotSelectedChatComponent } from './not-selected-chat/not-selected-chat.component';
import { SearchResultComponent } from './search-result/search-result.component';

@NgModule({
    declarations: [
        LoginComponent,
        ChatsComponent,
        CreateChatComponent,
        UserComponent,
        UserChatComponent,
        CreateGroupContactsComponent,
        CreateGroupDescriptionComponent,
        GroupComponent,
        EditGroupComponent,
        GroupChatComponent,
        AddToGroupComponent,
        MenuComponent,
        LanguageComponent,
        NotificationsComponent,
        VersionComponent,
        SupportComponent,
        UpdateComponent,
        NotSelectedChatComponent,
        SearchResultComponent,
    ],
    imports: [CommonModule, SharedModule],
    exports: [
        LoginComponent,
        ChatsComponent,
        CreateChatComponent,
        UserComponent,
        UserChatComponent,
        CreateGroupContactsComponent,
        CreateGroupDescriptionComponent,
        GroupComponent,
        EditGroupComponent,
        GroupChatComponent,
        AddToGroupComponent,
        MenuComponent,
        LanguageComponent,
        NotificationsComponent,
        VersionComponent,
        SupportComponent,
        UpdateComponent,
        NotSelectedChatComponent,
        SearchResultComponent,
    ],
})
export class ScreensModule {}
