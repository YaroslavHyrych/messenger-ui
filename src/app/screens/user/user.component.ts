import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    copyCommand!: WeekButtonCommand;
    createChatCommand!: WeekButtonCommand;

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.copyCommand = {
            key: 'copy',
            icon: 'file_copy',
            tooltip: 'Копіювати',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.createChatCommand = {
            key: 'create_chat',
            icon: 'forum',
            text: 'Створити чат',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }
}
