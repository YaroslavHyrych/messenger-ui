import { Component, OnInit } from '@angular/core';
import { WeekCommandCallback } from '@weekkit/web-components';
import { ChatsComponent } from '../chats/chats.component';
import { Contact, User } from '../create-chat/create-chat.component';

@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss'],
})
export class SearchResultComponent extends ChatsComponent implements OnInit {
    contacts!: Contact[];

    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.contacts = this._makeContacts();
    }

    private _makeContacts(): Contact[] {
        const contacts: Contact[] = [];
        for (let i: number = 0; i < 20; i++) {
            const user: User = {
                id: i,
                name: 'Кашуба Дмитро Анатолійович',
                isOnline: false,
                position: 'програміт прикладний провідний',
            };
            contacts.push({
                user,
                command: {
                    key: `navigate-${i}`,
                    handler: (resolve: WeekCommandCallback) => {
                        resolve();
                    },
                },
            });
        }
        return contacts;
    }
}
