import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    formGroup!: FormGroup;

    ngOnInit(): void {
        this._initCommands();
        this._initForm();
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }

    private _initForm(): void {
        this.formGroup = new FormGroup({
            showMessage: new FormControl(true),
            showMessageText: new FormControl(true),
            messageSound: new FormControl(true),
        });
    }
}
