import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

import { CreateGroupContactsComponent } from '../create-group-contacts/create-group-contacts.component';

@Component({
    selector: 'app-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss'],
})
export class GroupComponent extends CreateGroupContactsComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    writeMessageCommand!: WeekButtonCommand;
    menuCommand!: WeekButtonCommand;
    menuCommands!: WeekButtonCommand[];

    get hasUsers(): boolean {
        return !!this.contacts.length;
    }

    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this._initHeaderCommands();
    }

    private _initHeaderCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.writeMessageCommand = {
            key: 'write_message',
            icon: 'mode_comment',
            text: 'Написати',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.menuCommand = {
            key: 'menu',
            icon: 'more_horiz',
            tooltip: 'Меню',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.menuCommands = [
            {
                key: 'edit',
                icon: 'create',
                text: 'Редагувати',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'fixed',
                icon: 'turned_in',
                text: 'Зафіксувати',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'add_user',
                icon: 'person_add',
                text: 'Додати',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'delete',
                icon: 'delete',
                text: 'Видалити',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
        ];
    }
}
