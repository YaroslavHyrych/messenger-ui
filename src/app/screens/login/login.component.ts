import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    languageMenuCommand!: WeekButtonCommand;
    languageCommands!: WeekButtonCommand[];
    submitCommand!: WeekButtonCommand;
    formGroup!: FormGroup;

    ngOnInit(): void {
        this.languageMenuCommand = {
            key: 'menu',
            icon: 'language',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.languageCommands = [
            {
                key: 'ukr',
                icon: 'language',
                text: 'Українська',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Change to: Українська');
                },
            },
            {
                key: 'rus',
                icon: 'language',
                text: 'Русский',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Change to: Русский');
                },
            },
            {
                key: 'eng',
                icon: 'language',
                text: 'English',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Change to: English');
                },
            },
        ];
        this.submitCommand = {
            key: 'submit',
            text: 'АВОТРИЗУВАТИСЬ',
            isSubmit: true,
            processingIcon: 'spinner',
            successIcon: 'check',
            failIcon: 'error',
            handler: (resolve: WeekCommandCallback) => {
                Object.keys(this.formGroup.controls).forEach((key: string) => {
                    const control: AbstractControl = this.formGroup.controls[key];
                    control.updateValueAndValidity();
                });

                if (this.formGroup.invalid) {
                    resolve(false);
                    return;
                }
                resolve(true);
            },
        };
        this.formGroup = new FormGroup({
            login: new FormControl(null, Validators.required),
            password: new FormControl(null, Validators.required),
            remember: new FormControl(true),
        });
    }
}
