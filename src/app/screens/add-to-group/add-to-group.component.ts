import { Component, OnInit } from '@angular/core';

import { CreateGroupContactsComponent } from '../create-group-contacts/create-group-contacts.component';

@Component({
    selector: 'app-add-to-group',
    templateUrl: './add-to-group.component.html',
    styleUrls: ['./add-to-group.component.scss'],
})
export class AddToGroupComponent extends CreateGroupContactsComponent implements OnInit {
    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.nextCommand.text = 'Додати';
    }
}
