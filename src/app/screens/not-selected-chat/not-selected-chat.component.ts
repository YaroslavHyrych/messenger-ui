import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-selected-chat',
  templateUrl: './not-selected-chat.component.html',
  styleUrls: ['./not-selected-chat.component.scss']
})
export class NotSelectedChatComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
