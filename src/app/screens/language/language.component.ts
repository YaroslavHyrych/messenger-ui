import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-language',
    templateUrl: './language.component.html',
    styleUrls: ['./language.component.scss'],
})
export class LanguageComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    options!: Option[];

    private _selectedId: string = 'ukr';

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
        this._initOptions();
    }

    checkIsSelected(option: Option): boolean {
        return this._selectedId === option.id;
    }

    makeIconPath(isSelected: boolean): string {
        return isSelected ? 'check_circle' : 'radio_button_unchecked';
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }
    private _initOptions(): void {
        this.options = [
            {
                id: 'ukr',
                text: 'Українська',
                subtext: 'Ukrainian',
                command: {
                    key: 'ukr',
                    handler: (resolve: WeekCommandCallback) => {
                        this._selectedId = 'ukr';
                        resolve();
                    },
                },
            },
            {
                id: 'rus',
                text: 'Русский',
                subtext: 'Russian',
                command: {
                    key: 'rus',
                    handler: (resolve: WeekCommandCallback) => {
                        this._selectedId = 'rus';
                        resolve();
                    },
                },
            },
            {
                id: 'eng',
                text: 'English',
                subtext: 'English',
                command: {
                    key: 'eng',
                    handler: (resolve: WeekCommandCallback) => {
                        this._selectedId = 'eng';
                        resolve();
                    },
                },
            },
        ];
    }
}

interface Option {
    id: string;
    text: string;
    subtext: string;
    command: WeekCommand;
}
