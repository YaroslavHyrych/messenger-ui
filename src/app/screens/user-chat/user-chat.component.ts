import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-user-chat',
    templateUrl: './user-chat.component.html',
    styleUrls: ['./user-chat.component.scss'],
})
export class UserChatComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    infoCommand!: WeekButtonCommand;
    menuCommand!: WeekButtonCommand;
    menuCommands!: WeekButtonCommand[];

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
    }

    sendhandler(messageObj: Object): void {
        console.log(messageObj);
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.menuCommand = {
            key: 'menu',
            icon: 'more_horiz',
            tooltip: 'Меню',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.infoCommand = {
            key: 'info',
            text: 'Гирич Ярослав Володимирович',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.menuCommands = [
            { ...this.infoCommand, icon: 'info', text: 'Інфо' },
            {
                key: 'fixed',
                icon: 'turned_in',
                text: 'Зафіксувати',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'create_group',
                icon: 'people',
                text: 'Створити групу',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'delete',
                icon: 'delete',
                text: 'Видалити',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
        ];
    }
}
