import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

import { Contact, User } from '../create-chat/create-chat.component';

@Component({
    selector: 'app-create-group-description',
    templateUrl: './create-group-description.component.html',
    styleUrls: ['./create-group-description.component.scss'],
})
export class CreateGroupDescriptionComponent implements OnInit {
    nameControl!: FormControl;
    backCommand!: WeekButtonCommand;
    createCommand!: WeekButtonCommand;
    users!: User[];

    get hasUsers(): boolean {
        return !!this.users.length;
    }

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
        this.nameControl = new FormControl(null);
        this.users = this._makeUsers();
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };

        this.createCommand = {
            key: 'create',
            text: 'Створити',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }

    private _makeUsers(): User[] {
        const users: User[] = [];
        for (let i: number = 0; i < 20; i++) {
            const user: User = {
                id: i,
                name: 'Кашуба Дмитро Анатолійович',
                isOnline: false,
                position: 'програміт прикладний провідний',
            };
            users.push(user);
        }
        return users;
    }
}
