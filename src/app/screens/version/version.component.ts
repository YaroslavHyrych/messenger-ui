import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-version',
    templateUrl: './version.component.html',
    styleUrls: ['./version.component.scss'],
})
export class VersionComponent implements OnInit {
    backCommand!: WeekButtonCommand;
    updateCommand!: WeekButtonCommand;

    constructor() {}

    ngOnInit(): void {
        this._initCommands();
    }

    private _initCommands(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.updateCommand = {
            key: 'update',
            text: 'ОНОВИТИ',
            icon: 'sync',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }
}
