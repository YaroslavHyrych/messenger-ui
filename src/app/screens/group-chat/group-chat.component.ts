import { Component, OnInit } from '@angular/core';

import { UserChatComponent } from '../user-chat/user-chat.component';

@Component({
    selector: 'app-group-chat',
    templateUrl: './group-chat.component.html',
    styleUrls: ['./group-chat.component.scss'],
})
export class GroupChatComponent extends UserChatComponent implements OnInit {
    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.infoCommand.text = 'NDF заявки';
        this.menuCommands[2] = { ...this.menuCommands[2], icon: 'person_add', text: 'Додати' };
    }
}
