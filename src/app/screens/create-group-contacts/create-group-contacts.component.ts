import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

import { Contact, User } from '../create-chat/create-chat.component';

@Component({
    selector: 'app-create-group-contacts',
    templateUrl: './create-group-contacts.component.html',
    styleUrls: ['./create-group-contacts.component.scss'],
})
export class CreateGroupContactsComponent implements OnInit {
    searchControl!: FormControl;
    searchClearCommand?: WeekButtonCommand;
    isSearchClearVisible!: boolean;

    backCommand!: WeekButtonCommand;
    nextCommand!: WeekButtonCommand;
    addCommand!: WeekButtonCommand;

    contacts!: Contact[];
    selected: User[] = [];

    constructor() {}

    ngOnInit(): void {
        this._initSearch();
        this._initBackCommand();
        this._initCommands();
        this.contacts = this._makeContacts();
    }

    makeChipsCommand(user: User): WeekButtonCommand {
        return {
            key: 'remove',
            icon: 'clear',
            text: user.name,
            tooltip: 'Remove',
            handler: (resolve: WeekCommandCallback) => {
                const index: number = this.selected.findIndex((_: User) => _.id === user.id);
                this.selected.splice(index, 1);
                resolve();
            },
        };
    }

    makeIcon(isSelected: boolean): string {
        return isSelected ? 'check_circle' : 'radio_button_unchecked';
    }

    checkIsSelected(user: User): boolean {
        return this._findSelectedIndex(user) !== -1;
    }

    private _initSearch(): void {
        this.searchControl = new FormControl();
        this.searchControl.valueChanges
            .pipe(debounceTime(1000))
            .subscribe((value: string) => (this.isSearchClearVisible = !!value));
        this.searchClearCommand = {
            key: 'clear',
            icon: 'clear',
            tooltip: 'Clear',
            isDisabled: false,
            handler: (resolve: WeekCommandCallback) => {
                resolve(true);
                console.log('Search value:', this.searchControl.value);
                this.searchControl.setValue(null);
            },
        };
    }

    private _initBackCommand(): void {
        this.backCommand = {
            key: 'back',
            icon: 'arrow_back_ios',
            tooltip: 'Back',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }

    private _initCommands(): void {
        this.nextCommand = {
            key: 'next',
            text: 'Далі',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }

    private _makeContacts(): Contact[] {
        const contacts: Contact[] = [];
        for (let i: number = 0; i < 20; i++) {
            const user: User = {
                id: i,
                name: 'Кашуба Дмитро Анатолійович',
                isOnline: false,
                position: 'програміт прикладний провідний',
            };
            contacts.push({
                user,
                command: {
                    key: `select-${i}`,
                    handler: (resolve: WeekCommandCallback) => {
                        const index: number = this._findSelectedIndex(user);
                        if (index === -1) {
                            this.selected.push(user);
                        } else {
                            this.selected.splice(index, 1);
                        }
                        resolve();
                    },
                },
            });
        }
        return contacts;
    }

    private _findSelectedIndex(user: User): number {
        return this.selected.findIndex((_: User) => _.id === user.id);
    }
}
