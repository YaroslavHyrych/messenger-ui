import { Component, OnInit } from '@angular/core';
import { WeekCommandCallback } from '@weekkit/web-components';

import { Contact, User } from '../create-chat/create-chat.component';
import { CreateGroupDescriptionComponent } from '../create-group-description/create-group-description.component';

@Component({
    selector: 'app-edit-group',
    templateUrl: './edit-group.component.html',
    styleUrls: ['./edit-group.component.scss'],
})
export class EditGroupComponent extends CreateGroupDescriptionComponent implements OnInit {
    contacts!: Contact[];

    get hasUsers(): boolean {
        return !!this.contacts.length;
    }

    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.nameControl.setValue('NDF заявки');
        this.createCommand.text = 'Готово';
        this.contacts = this._makeContacts();
    }

    private _makeContacts(): Contact[] {
        const contacts: Contact[] = [];
        for (let i: number = 0; i < 20; i++) {
            const user: User = {
                id: i,
                name: 'Кашуба Дмитро Анатолійович',
                isOnline: false,
                position: 'програміт прикладний провідний',
            };
            contacts.push({
                user,
                command: {
                    key: `select-${i}`,
                    handler: (resolve: WeekCommandCallback) => {
                        const index: number = this.contacts.findIndex((_: Contact) => _.user.id === user.id);
                        if (index !== -1) {
                            this.contacts.splice(index, 1);
                        }
                        resolve();
                    },
                },
            });
        }
        return contacts;
    }
}
