import { Component, OnInit } from '@angular/core';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

import { ChatsComponent } from '../chats/chats.component';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent extends ChatsComponent implements OnInit {
    logoutCommand!: WeekButtonCommand;
    navigationCommands!: WeekButtonCommand[];

    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.logoutCommand = {
            key: 'logout',
            icon: 'exit_to_app',
            tooltip: 'Logout',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.navigationCommands = [
            {
                key: 'go_to_profile',
                icon: 'person',
                text: 'Гирич Ярослав Володимирович',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'go_to_notifications',
                icon: 'notifications',
                text: 'Сповіщення та звук',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'go_to_language',
                icon: 'language',
                text: 'Мова',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'go_to_version',
                icon: 'sync',
                text: 'Версія',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
            {
                key: 'go_to_support',
                icon: 'contact_support',
                text: 'Підтримка',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                },
            },
        ];
    }
}
