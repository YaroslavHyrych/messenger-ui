import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeekButtonCommand, WeekCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-chats',
    templateUrl: './chats.component.html',
    styleUrls: ['./chats.component.scss'],
})
export class ChatsComponent implements OnInit {
    searchControl!: FormControl;
    searchClearCommand?: WeekButtonCommand;
    isSearchClearVisible!: boolean;

    chatPageCommand!: WeekButtonCommand;
    settingsPageCommand!: WeekButtonCommand;
    createMenuCommand!: WeekButtonCommand;
    createMenuOptions!: WeekButtonCommand[];

    chats!: Chat[];
    selectedChat?: string;

    constructor() {}

    ngOnInit(): void {
        this._initSearch();
        this._initMenuCommands();
        this._initTabsCommands();
        this._initChats();
    }

    searchFocusHandler(): void {
        this.isSearchClearVisible = true;
    }

    searchBlurHandler(): void {
        if (this.searchControl.value) return;

        this.isSearchClearVisible = false;
    }

    private _initSearch(): void {
        this.searchControl = new FormControl();
        this.searchControl.valueChanges.subscribe(console.log.bind(undefined, 'Seach value changed:'));
        this.searchClearCommand = {
            key: 'clear',
            icon: 'clear',
            tooltip: 'Clear',
            isDisabled: false,
            handler: (resolve: WeekCommandCallback) => {
                resolve(true);
                console.log('Search value:', this.searchControl.value);
                this.searchControl.setValue(null);
            },
        };
    }

    private _initMenuCommands(): void {
        this.createMenuCommand = {
            key: 'create',
            icon: 'create',
            tooltip: 'Add',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.createMenuOptions = [
            {
                key: 'create_chat',
                icon: 'person',
                text: 'Чат',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Create chat');
                },
            },
            {
                key: 'create_group',
                icon: 'people',
                text: 'Групу',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Crate group');
                },
            },
            {
                key: 'find_group',
                icon: 'search',
                text: 'Пошук групи',
                handler: (resolve: WeekCommandCallback) => {
                    resolve();
                    console.log('Find group');
                },
            },
        ];
    }

    private _initTabsCommands(): void {
        this.chatPageCommand = {
            key: 'chats',
            icon: 'forum',
            tooltip: 'Chats',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
        this.settingsPageCommand = {
            key: 'settings',
            icon: 'settings',
            tooltip: 'Settings',
            handler: (resolve: WeekCommandCallback) => {
                resolve();
            },
        };
    }

    private _initChats(): void {
        this.chats = [];
        for (let i: number = 0; i < 15; i++) {
            this.chats.push({
                user: {
                    name: 'Кашуба Дмитро Анатолійович',
                    isOnline: false,
                    isGroup: false,
                },
                isUnread: false,
                lastMessage: 'Коли будуть готові макети?',
                command: {
                    key: `select-${i}`,
                    handler: (resolve: WeekCommandCallback) => {
                        this.selectedChat = `select-${i}`;
                        resolve();
                    },
                },
            });
        }
    }
}

interface Chat {
    user: {
        name: string;
        isOnline?: boolean;
        isGroup?: boolean;
    };
    isUnread?: boolean;
    lastMessage: string;
    command: WeekCommand;
}
