import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    WeekSwitchModule,
    WeekButtonModule,
    WeekIconModule,
    WeekInfoModule,
    WeekInputModule,
    WeekMenuModule,
    WeekActionModule,
} from '@weekkit/web-components';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './components/search/search.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { UserItemComponent } from './components/user-item/user-item.component';
import { MessageInputComponent } from './components/message-input/message-input.component';
import { MessageComponent } from './components/message/message.component';

@NgModule({
    declarations: [
        SearchComponent,
        AvatarComponent,
        UserItemComponent,
        MessageInputComponent,
        MessageComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        WeekInputModule,
        WeekButtonModule,
        WeekMenuModule,
        WeekIconModule,
        WeekInfoModule,
        WeekInputModule,
        WeekSwitchModule,
        WeekActionModule,
    ],
    exports: [
        ReactiveFormsModule,
        WeekButtonModule,
        WeekMenuModule,
        WeekIconModule,
        WeekInfoModule,
        WeekInputModule,
        WeekSwitchModule,
        SearchComponent,
        AvatarComponent,
        UserItemComponent,
        MessageInputComponent,
        MessageComponent,
        WeekActionModule,
    ],
})
export class SharedModule {}
