import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { WeekViewMode } from '@weekkit/web-components';

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit, AfterViewInit {
    @Input() text!: string;
    @Input() time!: string;
    @Input() name?: string;
    @Input() isInput: boolean = false;

    get infoMode(): WeekViewMode {
        return this.isInput ? 'light' : 'dark';
    }

    constructor(private readonly _elementRef: ElementRef, private readonly _renderer: Renderer2) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this._renderer.addClass(this._elementRef.nativeElement, this.isInput ? 'message_input' : 'message_output');
    }
}
