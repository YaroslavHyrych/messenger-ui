import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Component({
    selector: 'app-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit, AfterViewInit {
    @Input() size: AvatarSize = 'middle';
    @Input() isGroup?: boolean = false;
    @Input() isOnline?: boolean = false;
    @Input() isMarked?: boolean = false;
    @Input() name!: string;

    get shortName(): string {
        const paths: string[] = this.name.split(' ');
        return paths.length > 1 ? `${paths[0][0]}${paths[1][0]}` : `${paths[0].substr(0, 2)}`;
    }

    constructor(private readonly _renderer: Renderer2, private readonly _elementRef: ElementRef) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        const classes = ['avatar', `avatar_${this.size}`];
        classes.forEach((className: string) => this._renderer.addClass(this._elementRef.nativeElement, className));
    }
}

export type AvatarSize = 'big' | 'middle' | 'small';
