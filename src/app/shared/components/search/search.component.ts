import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeekCommand } from '@weekkit/web-components';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
    @Input() control!: FormControl;
    @Input() clearCommand?: WeekCommand;
    @Input() isClearVisible?: boolean;
    @Output() onFocus: EventEmitter<void> = new EventEmitter();
    @Output() onBlur: EventEmitter<void> = new EventEmitter();

    constructor() {}

    ngOnInit(): void {}

    searchBlurHandler(): void {
        this.onBlur.emit();
    }

    searchFocusHandler(): void {
        this.onFocus.emit();
    }
}
