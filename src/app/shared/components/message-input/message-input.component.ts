import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { WeekButtonCommand, WeekCommandCallback } from '@weekkit/web-components';

@Component({
    selector: 'app-message-input',
    templateUrl: './message-input.component.html',
    styleUrls: ['./message-input.component.scss'],
})
export class MessageInputComponent implements OnInit {
    @Output() onSend: EventEmitter<Object> = new EventEmitter();
    sendCommand!: WeekButtonCommand;
    formGroup!: FormGroup;

    constructor() {}

    ngOnInit(): void {
        this.formGroup = new FormGroup({
            message: new FormControl(null),
        });
        this.sendCommand = {
            key: 'send',
            icon: 'send',
            tooltip: 'Відправити',
            isSubmit: true,
            handler: (resolve: WeekCommandCallback) => {
                resolve();
                if (this.formGroup.invalid) return;
                this.onSend.next(this.formGroup.value);
            },
        };
    }
}
