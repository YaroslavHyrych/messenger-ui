import { Component, Input, OnInit } from '@angular/core';

import { AvatarSize } from '../avatar/avatar.component';

@Component({
    selector: 'app-user-item',
    templateUrl: './user-item.component.html',
    styleUrls: ['./user-item.component.scss'],
})
export class UserItemComponent implements OnInit {
    @Input() isOnline?: boolean;
    @Input() isGroup?: boolean;
    @Input() isMarked?: boolean;
    @Input() avatarSize: AvatarSize = 'middle';
    @Input() text!: string;
    @Input() subtext!: string;

    constructor() {}

    ngOnInit(): void {}
}
